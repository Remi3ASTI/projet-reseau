import tkinter as tk
import json
from my_socket import SocketCommunication


class JSONSender:
    def __init__(self, root):
        self.root = root
        self.root.title("JSON Sender")

        self.socket_comm = SocketCommunication()

        self.load_json_data()

        self.send_button = tk.Button(self.root, text="Envoyer", command=self.send_json)
        self.send_button.pack(pady=10)

        self.root.protocol("WM_DELETE_WINDOW", self.close_window)
        self.root.mainloop()

    def close_window(self):
        self.socket_comm.close_connection()
        self.root.destroy()

    def load_json_data(self):
        try:
            with open("exemple.json", "r") as json_file:
                self.json_data = json.load(json_file)
        except FileNotFoundError:
            print("Le fichier 'exemple.json' n'a pas été trouvé.")
            self.json_data = {}

    def send_json(self):
        self.load_json_data()
        json_str = json.dumps(self.json_data)
        self.socket_comm.send_data(json_str)

if __name__ == "__main__":
    root = tk.Tk()
    JSONSender(root)
    