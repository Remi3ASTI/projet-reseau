import socket
import json

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('localhost', 12345))
server_socket.listen()

print("Attente de la connexion du client...")
client_socket, client_address = server_socket.accept()
print("Connexion etablie avec", client_address)

server_position_data = {"x": 15, "y": 15}
client_socket.send(json.dumps(server_position_data).encode())

client_received_data = client_socket.recv(1024).decode()
client_position_data = json.loads(client_received_data)
client_x, client_y = client_position_data["x"], client_position_data["y"]

print("position du client : " , client_x, client_y)