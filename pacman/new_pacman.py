import pygame 
import sys
import random
import time
import json
import threading
from new_parameters import *
from new_game_methods import *
from Menu import MenuPrincipal
from my_socket import SocketCommunication


menu = MenuPrincipal()
menu.mainloop()
# Initialisation de Pygame
pygame.init()

# Paramètres de la fenêtre

# Initialisation Réseau

 
# Création de la fenêtre
fenetre = pygame.display.set_mode((largeur_fenetre, hauteur_fenetre))
pygame.display.set_caption("Pac-Man")

# Initialisation du labyrinthe  
sol, murs, points = [], [], []
coords_sol,coords_murs = [],[]

init_labyrinth(sol, murs, points, labyrinthe)
for rectangle in sol:
    pygame.draw.rect(fenetre, noir, rectangle)
for rectangle in murs:
    pygame.draw.rect(fenetre, blanc, rectangle)

for y, ligne in enumerate(labyrinthe):
    for x, case in enumerate(ligne):
        if case == 1:
            coords_sol.append((x,y))
        if case == 0:
            coords_murs.append((x,y))


joueurs = []
pacmans = {}
joueurs_ownership = {menu.pseudo:[]}

for coord in coords_sol:
    joueurs_ownership[menu.pseudo].append(coord)


pos_x,pos_y = pos_init_x, pos_init_y
pacman = pygame.Rect(pos_init_x, pos_init_y, 25, 25)
pseudo = menu.pseudo
print(pseudo)
score = 0
order = "game"

key_a_pressed = False
key_z_pressed = False
key_e_pressed = False



# Utilisez la fonction convert_to_dict pour convertir le JSON en dictionnaire

# Boucle principale du jeu


if menu.mode== "multijoueur":
    json_data = json_envoi(int(pos_x), int(pos_y), pseudo, score, "share",menu.pseudo)
    json_str = json_data
    menu.socket.send_data(json_str)

running = True



while running:    
    # Mise à jour des évènements pygame
    pygame_events()
    
    # Mise à jour de la position
    touches = pygame.key.get_pressed()
    pos_x,pos_y = deplacements_et_collisions(pos_x, pos_y, pacman, murs, touches) 
    pos_x, pos_y = traverser_bords(pos_x, pos_y)
    pacman = pygame.Rect(pos_x, pos_y, 25, 25)
    recuperer_points(points, pacman,pacmans,joueurs)




    if menu.mode == "multijoueur" or menu.mode == "multijoueur2": 
        data = menu.socket.receive_data()
        if data is not None:
            print("Received data:", data)
            # Split the concatenated JSON objects
            json_objects = data.split("}{")
            # Process each JSON object individually
            for json_str in json_objects:
                # Add back the missing braces for each object
                if not json_str.startswith("{"):
                    json_str = "{" + json_str
                if not json_str.endswith("}"):
                    json_str += "}"
                # Parse and process the JSON object
                try:
                    data_dict = json.loads(json_str)

                    if data_dict["order"]=="connect" : 
                        #envoyer au joueur qui a envoyé -> point, coordo de base
                        print("JOIE")

                    if data_dict["order"]=="game" and data_dict["data"]["delete"] != "" and data_dict["data"]["pseudo"] != pseudo: 
                        points.remove(data_dict["data"]["delete"])

                    if data_dict["order"]=="game" and data_dict["data"]["pseudo"] != pseudo: 
                        #envoyer au joueur qui a envoyé -> point, coordo de base
                        position_x = data_dict["data"]["pos_x"]
                        position_y= data_dict["data"]["pos_y"]
                        if data_dict["data"]["pseudo"] not in joueurs:
                            joueurs.append(data_dict["data"]["pseudo"])
                        pacmans[data_dict["data"]["pseudo"]] = pygame.Rect(position_x, position_y, 25, 25)
                    if data_dict["order"]=="game" and data_dict["data"]["deconnexion"]=="disconnect": 
                        print(joueurs,pacmans)
                        joueurs.remove(data_dict["data"]["pseudo"])
                        del pacmans[data_dict["data"]["pseudo"]]
                        print(joueurs,pacmans)


                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON: {e}",json_str)
        
    # envoi(touches, pos_x, pos_y, pseudo)
                    
        if touches[pygame.K_UP]:
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "game",menu.pseudo)
            json_str = json_data
            menu.socket.send_data(json_str)
        if touches[pygame.K_DOWN]:
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "game",menu.pseudo)
            json_str = json_data
            menu.socket.send_data(json_str)
        if touches[pygame.K_LEFT]:
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "game",menu.pseudo)
            json_str = json_data
            menu.socket.send_data(json_str)
        if touches[pygame.K_RIGHT]:
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "game",menu.pseudo)
            json_str = json_data
            menu.socket.send_data(json_str)
        if touches[pygame.K_SPACE]:
            print(pos_x, pos_y )
            print(position_x,position_y)
        if touches[pygame.K_ESCAPE]:
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "game",menu.pseudo,deconnexion="disconnect")
            json_str = json_data
            menu.socket.send_data(json_str)
            json_data = json_envoi(int(pos_x), int(pos_y), "", score, "disconnect",menu.pseudo)
            json_str = json_data
            menu.socket.send_data(json_str)
    

    # Récupération de la position des autres joueurs
 

    # Affichage du jeu
    dessin_jeu(fenetre, pacman, pacmans, joueurs, sol, murs, points)