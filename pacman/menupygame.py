import tkinter as tk
from tkinter import simpledialog

class MenuPrincipal(tk.Tk):
    def __init__(self):
        super().__init__()

        self.title("Menu Principal")
        self.geometry("300x200")

        self.label = tk.Label(self, text="Choisissez une option:", font=("Arial", 14))
        self.label.pack(pady=10)

        self.bouton_solo = tk.Button(self, text="Solo", command=self.lancer_jeu_solo)
        self.bouton_solo.pack(pady=5)

        self.bouton_multijoueur = tk.Button(self, text="Multijoueur", command=self.lancer_multijoueur)
        self.bouton_multijoueur.pack(pady=5)

        self.bouton_quitter = tk.Button(self, text="Quitter", command=self.quitter_jeu)
        self.bouton_quitter.pack(pady=5)

    def lancer_jeu_solo(self):
        self.mode = "solo"
        self.destroy()  # Fermer la fenêtre du menu principal

    def lancer_multijoueur(self):
        self.mode = "multijoueur"
        self.destroy()  # Fermer la fenêtre du menu principal

    def quitter_jeu(self):
        self.mode = "quitter"
        self.destroy()  # Fermer la fenêtre du menu principal

def main():
    menu = MenuPrincipal()
    menu.mainloop()

if __name__ == "__main__":
    main()
