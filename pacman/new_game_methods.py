from new_parameters import *
import pygame 
import sys
import json


def init_labyrinth(sol, murs, points, labyrinthe):
    for i in range(len(labyrinthe)):
        for j in range(len(labyrinthe[i])):
            rect_x = j * taille_case
            rect_y = i * taille_case
            rect = pygame.Rect(rect_x, rect_y, taille_case, taille_case)
            point = pygame.Rect(rect_x + 2*taille_point, rect_y + 2*taille_point, taille_point, taille_point)
            if labyrinthe[i][j] == 0:
                murs.append(rect)
                color = blanc
            if labyrinthe[i][j] == 1:
                sol.append(rect)
                color = noir
                points.append(point)



def traverser_bords(pos_x, pos_y):
    if pos_x < 0:
        pos_x = largeur_fenetre
    elif pos_x > largeur_fenetre:
        pos_x = 0

    if pos_y < 0:
        pos_y = hauteur_fenetre
    elif pos_y > hauteur_fenetre:
        pos_y = 0
    return pos_x, pos_y

def deplacements_et_collisions(pos_x, pos_y, pacman, murs, touches):
    speed_x, speed_y = 0,0
    for mur in murs:
        if pacman.colliderect(mur) and (abs(mur.top - pacman.bottom) < collision_tolerance):
                speed_y = 0
                speed_x = 0
        if pacman.colliderect(mur) and (abs(mur.bottom - pacman.top) < collision_tolerance):
                speed_y = 0
                speed_x = 0
        if pacman.colliderect(mur) and (abs(mur.left - pacman.right) < collision_tolerance):
                speed_y = 0
                speed_x = 0
        if pacman.colliderect(mur) and (abs(mur.right - pacman.left) < collision_tolerance):
                speed_y = 0
                speed_x = 0
        if touches[pygame.K_DOWN]:
            if pacman.colliderect(mur) and (abs(mur.top - pacman.bottom) < collision_tolerance):
                speed_y = 0
                speed_x = 0
                break
            speed_y = speed
            speed_x = 0
        elif touches[pygame.K_UP]:
            if pacman.colliderect(mur) and (abs(mur.bottom - pacman.top) < collision_tolerance):
                speed_y = 0
                speed_x = 0
                break
            speed_y = -speed
            speed_x = 0
        elif touches[pygame.K_RIGHT]:
            if pacman.colliderect(mur) and (abs(mur.left - pacman.right) < collision_tolerance):
                speed_y = 0
                speed_x = 0
                break
            speed_x = speed
            speed_y = 0
        elif touches[pygame.K_LEFT]: 
            if pacman.colliderect(mur) and (abs(mur.right - pacman.left) < collision_tolerance):
                speed_y = 0
                speed_x = 0
                break
            speed_x = -speed
            speed_y = 0       



    pos_x += speed_x 
    pos_y += speed_y 
    return pos_x, pos_y


def recuperer_points(points, pacman,pacmans,joueurs):
    for point in points:
        if pacman.colliderect(point):
            points.remove(point)
        for joueur in joueurs:
            if pacmans[joueur].colliderect(point): points.remove(point)


def dessin_jeu(fenetre, pacman, pacmans, joueurs, sol, murs, points):
    for rectangle in sol:
        pygame.draw.rect(fenetre, noir, rectangle)
    for rectangle in murs:
        pygame.draw.rect(fenetre, blanc, rectangle)
    for point in points:
        pygame.draw.rect(fenetre, color_point, point)    
    for joueur in joueurs:
        pygame.draw.circle(fenetre, jaune, (pacmans[joueur].x + 25//2, pacmans[joueur].y + 25//2), 15)
    
    pygame.draw.circle(fenetre, jaune, (pacman.x + 25//2, pacman.y + 25//2), 15)


    pygame.display.flip()

def pygame_events():
     for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

def convert_to_json(dictionary):
    try:
        json_data = json.dumps(dictionary, indent=2)
        return json_data
    except Exception as e:
        return f"Erreur lors de la conversion en JSON : {str(e)}"
    
def convert_to_dict(json_data):
    try:
        dictionary = json.loads(json_data)
        return dictionary
    except Exception as e:
        return f"Erreur lors de la conversion en dictionnaire : {str(e)}"

    
def json_envoi(x, y, pseudo, score, order, ownpseudo,deconnexion="",point=""):

        json_data = {
        "order": order,
        "pseudo": pseudo,
        "data": {
            "pseudo": ownpseudo,
            "score": score,
            "pos_x": x,
            "pos_y": y,
            "case_x": x//35,
            "case_y": y//35,
            "deconnexion": deconnexion,
            "delete": point,
            }
        }       

 
        #print(convert_to_json(json_data))
        #json_str = json.dump(json_data)
        #socket.send_data(json_data)
        return convert_to_json(json_data)



def json_recup(json_data):

    data = convert_to_dict(json_data)
    #print(convert_to_dict(json_data))

    new_pos_x = data["data"]["pos_x"]
    new_pos_y = data["data"]["pos_y"]
    return data
    #print("new x : ", new_pos_x)
    #print("new y : ", new_pos_y)

    

