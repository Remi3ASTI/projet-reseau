import tkinter as tk
from tkinter import messagebox, simpledialog
from new_game_methods import *
from my_socket import SocketCommunication
from json import *
import ijson
import io

class MenuPrincipal(tk.Tk):
    def __init__(self):
        super().__init__()

        self.title("Menu Principal")
        self.geometry("300x200")
        self.pseudo = ""
        self.mode = ""
        self.available_games = set() # va stocker les parties dispos
        self.label = tk.Label(self, text="Choisissez une option:", font=("Arial", 14))
        self.label.pack(pady=10)
        self.socket = None
        self.bouton_solo = tk.Button(self, text="Solo", command=self.lancer_jeu_solo)
        self.bouton_solo.pack(pady=5)

        self.bouton_multijoueur = tk.Button(self, text="Multijoueur", command=self.lancer_multijoueur)
        self.bouton_multijoueur.pack(pady=5)

        self.bouton_quitter = tk.Button(self, text="Quitter", command=self.quitter_jeu)
        self.bouton_quitter.pack(pady=5)

        self.bouton_quitter = tk.Button(self, text="Chercher", command=self.chercher_jeu)
        self.bouton_quitter.pack(pady=5)

    def lancer_jeu_solo(self):
        self.destroy()  # Fermer la fenêtre du menu principal
#        os.system("python3 new_pacman.py solo")  # Démarrer le jeu en mode solo
        

    def lancer_multijoueur(self):
        pseudo = simpledialog.askstring("Pseudo", "Veuillez entrer votre pseudo:")
        if pseudo:
            # Si un pseudo est saisi, lancer le jeu en mode multijoueur
            self.destroy()  # Fermer la fenêtre du menu principal
            #os.system(f"python3 new_pacman.py multijoueur {pseudo}") 
            self.mode = "multijoueur"
            self.pseudo = pseudo
            self.socket = SocketCommunication()

    def update_game_list(self, game_data):
        if game_data not in self.available_games:
            self.available_games.add(game_data)
            self.search_listbox.insert(tk.END, game_data)

    def search_loop(self):
        if self.search_loop_active:
            json_data = json_envoi(25, 25, self.pseudo, 0, "search",self.pseudo)
            self.socket.send_data(json_data)
            data = self.socket.receive_data()
            if data is not None:
                print("Received data:", data)
                # Split the concatenated JSON objects
                json_objects = data.split("}{")
                # Process each JSON object individually
                for json_str in json_objects:
                    # Add back the missing braces for each object
                    if not json_str.startswith("{"):
                        json_str = "{" + json_str
                    if not json_str.endswith("}"):
                        json_str += "}"
                    # Parse and process the JSON object
                    try:
                        data_dict = json.loads(json_str)

                        if data_dict["order"]=="share" : self.update_game_list(data_dict["pseudo"])
                    except json.JSONDecodeError as e:
                        print(f"Error decoding JSON: {e}",json_str)
            self.after(1000, self.search_loop)

    def chercher_jeu(self):
        self.withdraw()  # pour masque un écran
        self.search_interface = tk.Toplevel()  # on stocke la ref
        self.search_interface.title("Recherche de partie en cours...")

        self.search_listbox = tk.Listbox(self.search_interface)
        self.search_listbox.pack(pady=10)
        stop_search_button = tk.Button(self.search_interface, text="STOP LA RECHERCHE", command=self.stop_search)
        stop_search_button.pack(pady=10)

        pseudo = simpledialog.askstring("Pseudo", "Veuillez entrer votre pseudo:")
        self.pseudo = pseudo
        self.mode = "search"
        self.pseudo = pseudo
        if self.socket == None: self.socket = SocketCommunication()
        self.search_loop_active = True
        self.search_loop()    
        self.search_listbox.bind("<Double-Button-1>", self.double_click_game)

    def double_click_game(self, event):
        # Récupérer l'élément sélectionné dans la liste des parties disponibles
        selected_index = self.search_listbox.curselection()
        if selected_index:
            selected_game = self.search_listbox.get(selected_index)
            print(selected_game,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

            # Créer le JSON correspondant et l'envoyer
            json_data = {
                "order": "connect",
                "pseudo": selected_game,
                "data":{"pseudo":self.pseudo}
                # Autres champs du JSON selon vos besoins
            }
            json_str = json.dumps(json_data)  # Convertir le dictionnaire en chaîne JSON
            self.socket.send_data(json_str)
            self.mode = "multijoueur2"
            self.destroy()


    def stop_search(self):
        json_data = json_envoi(25, 25, self.pseudo, 0, "stop_search",self.pseudo)
        self.socket.send_data(json_data)
        self.search_loop_active = False
        self.available_games.clear()
        self.search_interface.destroy()  
        self.deiconify()


    def quitter_jeu(self):
        self.destroy()  # Fermer la fenêtre du menu principal
        sys.exit()


def main():
    menu = MenuPrincipal()
    menu.mainloop()

if __name__ == "__main__":
    main()
    