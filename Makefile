CC = gcc
CFLAGS = -Wall -Wextra -I/home/user/Bureau/projet/projet-reseau/cJSON
LDFLAGS = -L/home/user/Bureau/projet/projet-reseau/cJSON
LDLIBS = -lcjson
PYTHON = python3
SRCS = main.c socket.c json_reader.c
OBJS = $(SRCS:.c=.o)

all: main

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

main: $(OBJS)
	$(CC) $(LDFLAGS) -o main $(OBJS) $(LDLIBS)

run: all
	./main &                 # arrière-plan
	$(PYTHON) main.py

clean:
	rm -f main $(OBJS)

.PHONY: all run clean
